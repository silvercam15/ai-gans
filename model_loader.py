import sys

import numpy as np

from keras.models import load_model

if __name__ == '__main__':
    samples = 20 
    args = sys.argv
    PATHNAME = None
    usageInfo = args[0] + " usage:\n \
    -m <FILEPATH>:  load generator from FILEPATH\n \
    -n <SAMPLES>:   generate SAMPLES sets of features (default 20)\n"
    for i, arg in enumerate(args):
        if (arg == '-h'):
            print usageInfo
            sys.exit()
        if (arg == '-m'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            PATHNAME = args[i+1]
        if (arg == '-n'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            samples = int(args[i+1])
    if (not PATHNAME):
        print "ERROR: Model FILEPATH must be specified"
        print usageInfo
        sys.exit()
    model = load_model(PATHNAME)
    noise = np.random.uniform(-1.0, 1.0, size=[samples, 100])
    features = model.predict(noise)
    for feats in features:
        for feat in feats[:-1]:
            print ("%.2f, " % feat),
        print ("%.2f" % feats[-1])
