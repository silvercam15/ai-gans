# Using GANs to synthesize novel instances of 3D models in the Autodesk Maya software

### Silvia Camara, James Holland, and Jeff Klouda

## Introduction
This project contains code to run a Generative Adversarial Network in order to generate 
novel instances of iris flowers as described by the UCI Iris Data Set to be translated
into 3D models and rendered using the Autodesk Maya software.

## Installation and dependencies
This code requires Python 2.x, the Python Theano, Keras, NumPy, and scikit-learn libraries,
and the Autodesk Maya software.

## Using the software

### Training

To run the GAN and train the discriminator and generator models, use Python to run the 
`iris_gan.py` script. The script can be run with the following options, all of which can
be viewed by running `iris_gan.py -h`:

`-g <FILEPATH>`: to save the generator model to FILEPATH.

`-d <FILEPATH>`: to save the discriminator model to FILEPATH.

`-p <NUMFEATURES>`: to specify the number of features the generator should generate after
training. By default, these features are printed to stdout.

`-s <FILEPATH>`: to save features to a file instead of printing to stdout.

`-n`: to normalize input features to values between 0 and 1. Generated features must later
be scaled by the output factor `n` to be used by Maya (this is automatically done when
printing or saving features using the `iris_gan.py` script.

`-t <ITERATIONS>`: to specify the number of training steps for which the GAN runs.

### Generating Features

To generate features using a saved generator model, use Python to run the `model_loader.py`
script. Features are printed to stdout in csv format. This script can be run with the 
following options:

`-m <FILEPATH>`: to specify the file from which to load the generator. NOTE: a file must be
specified to run the `model_loader.py` script.

`-n <SAMPLES>`: to specify the number of sets of features to generate.
