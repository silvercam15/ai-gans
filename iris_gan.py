'''
Silvia Camara
James Holland
Jeff Klouda

Based on code by Rowel Atienza at https://github.com/roatienza/Deep-Learning-Experiments/blob/master/Experiments/Tensorflow/GAN/dcgan_mnist.py

TODO: This version works about 20% of time. If either loss is constantly going
up by around iteration 3000, restart it.
Ways we could improve: adjust training/model parameters, normalize inputs/outputs 

'''

import sys

import numpy as np
import time
import keras
from sklearn import datasets

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Reshape
from keras.layers import Conv1D, Conv2D, Conv2DTranspose, UpSampling2D
from keras.layers import LeakyReLU, Dropout
from keras.layers import BatchNormalization
from keras.optimizers import Adam, RMSprop

class ElapsedTimer(object):
    def __init__(self):
        self.start_time = time.time()
    def elapsed(self,sec):
        if sec < 60:
            return str(sec) + " sec"
        elif sec < (60 * 60):
            return str(sec / 60) + " min"
        else:
            return str(sec / (60 * 60)) + " hr"
    def elapsed_time(self):
        print("Elapsed: %s " % self.elapsed(time.time() - self.start_time) )

class DCGAN(object):
    def __init__(self, num_feats=4, h_layer=20):

        self.D = None   # discriminator
        self.G = None   # generator
        self.AM = None  # adversarial model
        self.DM = None  # discriminator model
        self.num_feats = num_feats
        self.h_layer = h_layer

    def discriminator(self):
        #In: 4 x 1 features
        #Out: 1-dim probability
        if self.D:
            return self.D
        dropout = 0.4
        self.D=Sequential()
        self.D.add(Dense(self.h_layer, input_dim=self.num_feats, activation='sigmoid'))
        # self.D.add(Dense(8, input_dim=4, activation='relu'))
        '''
        # for classification
        self.D.add(Dense(3, activation='softmax'))
        '''
        self.D.add(Dense(1, activation='sigmoid'))
        self.D.summary()
        return self.D
    
    def generator(self):
        # Generate iris features from n-dimensional noise
        n = 100
        if self.G:
            return self.G
        self.G = Sequential()
        dropout = 0.4

        # In: 100 x 1 noise values 
        # Out: 4 x 1 iris features

        self.G.add(Dense(self.h_layer, input_dim=100, activation='sigmoid'))
        self.G.add(Dense(self.num_feats, activation='relu'))

        self.G.summary()
        return self.G

    def discriminator_model(self):
        if self.DM:
            return self.DM
        optimizer = RMSprop(lr=0.0002, decay=6e-8)
        self.DM = Sequential()
        self.DM.add(self.discriminator())
        self.DM.compile(loss='binary_crossentropy', optimizer=optimizer,\
            metrics=['accuracy'])
        return self.DM

    def adversarial_model(self):
        if self.AM:
            return self.AM
        optimizer = RMSprop(lr=0.0001, decay=3e-8)
        self.AM = Sequential()
        self.AM.add(self.generator())
        self.AM.add(self.discriminator())
        self.AM.compile(loss='binary_crossentropy', optimizer=optimizer,\
            metrics=['accuracy'])
        return self.AM

class IRIS_DCGAN(object):
    def __init__(self, normalize=False):

        iris_dataset=datasets.load_iris()
        iris_features = iris_dataset.data
        #iris_labels = iris_data[:, 4]

        self.normalize = normalize;

        n_value = 1 # for normalization
        if (self.normalize):
            dup = []
            for feats in iris_features:
                for feat in feats:
                    dup.append(feat)
            n_value = max(dup)
            for i, feats in enumerate(iris_features):
                for j, feat in enumerate(feats):
                    iris_features[i][j] = feat/n_value

        self.n_value = n_value
        self.x_train = iris_features
        
        self.DCGAN = DCGAN()
        self.discriminator =  self.DCGAN.discriminator_model()
        self.adversarial = self.DCGAN.adversarial_model()
        self.generator = self.DCGAN.generator()

    def train(self, train_steps=2000, batch_size=150, save_interval=0):
        noise_input = None
        if save_interval>0:
            noise_input = np.random.uniform(-1.0, 1.0, size=[16, 100])
        for i in range(train_steps):
            features_train = self.x_train[0:150]
            noise = np.random.uniform(-1.0, 1.0, size=[batch_size, 100])
            features_fake=self.generator.predict(noise)
            x = np.concatenate((features_train, features_fake))
            y = np.ones([2*batch_size, 1])
            y[batch_size:, :] = 0
            d_loss = self.discriminator.train_on_batch(x, y)

            y = np.ones([batch_size, 1])
            noise = np.random.uniform(-1.0, 1.0, size=[batch_size, 100])
            a_loss = self.adversarial.train_on_batch(noise, y)
            log_mesg = "%d: [D loss: %f, acc: %f]" % (i, d_loss[0], d_loss[1])
            log_mesg = "%s  [A loss: %f, acc: %f]" % (log_mesg, a_loss[0], a_loss[1])
            print(log_mesg)

    def print_features(self, filename="iris_fakes", save2file=False, fake=True, samples=16, noise=None, step=0):
        if fake:
            if noise is None:
                noise = np.random.uniform(-1.0, 1.0, size=[samples, 100])
            else:
                filename = 'iris_out%d.csv' % step
            features = self.generator.predict(noise) * self.n_value
        else:
            features = self.x_train
        if (self.normalize):
            print "n = ", self.n_value
        if (save2file):
            with open(filename, "w") as f:
                for feats in features:
                    for feat in feats[:-1]:
                        f.write("%.2f, " % feat)
                    f.write("%.2f\n" % feats[-1])
        else:
            for feats in features:
                for feat in feats[:-1]:
                    print ("%.2f," % feat),
                print ("%.2f" % feats[-1])

    def save_generator(self, filepath):
        self.generator.save(filepath)
    def save_discriminator(self, filename):
        self.discriminator.save(filepath)

if __name__ == '__main__':
    saveG = False
    saveD = False
    normalize = False
    saveFeatures = False
    featFile = "iris_fakes"
    printFeatures = True
    n_features = 20
    t_steps = 10000
    usageInfo = sys.argv[0] + " usage:\n \
    -g <FILEPATH>:    save generator to FILEPATH\n \
    -d <FILEPATH>:    save discriminator to FILEPATH\n \
    -p <NUMFEATURES>: generate NUMFEATURES sets of features (default 20)\n \
    -s <FILEPATH>:    save generated features to FILEPATH (instead of printing to STDOUT)\n \
    -n:               normalize features during training (generator outputs will be scaled down by factor n)\n \
    -t <ITERATIONS>:  run ITERATIONS training steps (default 10000)\n"
    args = sys.argv

    for i, arg in enumerate(args):
        if (arg == '-h'):
            print usageInfo
            sys.exit()
        if (arg == '-g'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            saveG = True
            gen_file = args[i+1]
        if (arg == '-d'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            saveD = True
            dis_file = args[i+1]
        if (arg == '-p'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            printFeatures = True
            n_features = int(args[i+1])
        if (arg == '-s'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            saveFeatures = True
            featFile = args[i+1]
        if (arg == '-t'):
            if (i+1 == len(args) or args[i+1][0] == '-'):
                print usageInfo
                sys.exit()
            t_steps = int(args[i+1])
        if (arg == '-n'):
            normalize = True

    iris_dcgan = IRIS_DCGAN(normalize=normalize)
    timer = ElapsedTimer()
    iris_dcgan.train(train_steps=t_steps, batch_size=150, save_interval=500)
    timer.elapsed_time()
    if (saveG):
        iris_dcgan.save_generator(gen_file)
    if (saveD):
        iris_dcgan.save_discriminator(dis_file)
    if (printFeatures):
        iris_dcgan.print_features(samples=n_features, save2file=saveFeatures, filename=featFile, fake=True)
