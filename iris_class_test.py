import os
os.environ['KERAS_BACKEND'] = 'theano'
os.environ['MKL_THREADING_LAYER'] = 'GNU'
import numpy
import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn import datasets
import numpy as np

seed = 7
numpy.random.seed(seed)

iris_dataset=datasets.load_iris()
iris_features = iris_dataset.data
iris_labels = iris_dataset.target 

X = iris_features
Y = iris_labels

'''
dataframe = pandas.read_csv("iris/iris.data", header=None)
dataset = dataframe.values
X = dataset[:, 0:4].astype(float)
Y = dataset[:,4]

'''

# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)

#define baseline_model
def baseline_model():
    #create model
    model = Sequential()
    model.add(Dense(8, input_dim=4, activation='relu'))
    model.add(Dense(3, activation='softmax'))
    model.summary()
    #compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

estimator = KerasClassifier(build_fn=baseline_model, epochs=200, batch_size=5, verbose=0)


kfold = KFold(n_splits=10, shuffle=True, random_state=seed)

#results = cross_val_score(estimator, X, dummy_y, cv=kfold)
#print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))

X_gen = [[4.950651,3.3821402,1.4717722,0.16581187],
 [5.243519,3.575719,1.53612,0.22733843],
 [4.723212,3.322461,1.5828236,0.35774052],
 [4.9421825,3.075039,1.5988753,0.34602892],
 [4.5500464,3.546545,1.3230218,0.2758475 ],
 [4.462473,3.505083,1.2381462,0.17675504],
 [4.964015,3.138403,1.3913121,0.37350848],
 [4.66727,3.0968006,1.3887622,0.28871548],
 [5.0879035,3.467287,1.4758028,0.26359785],
 [5.0772605,3.44700,1.7849288,0.26127806],
         [4.8435335,3.5657003,1.4784226,0.3010547 ],
 [4.775977, 3.2055893,1.3023047,0.3132078 ],
 [4.8774233,3.3066773,1.6452328,0.1832655 ],
 [4.65289,3.326058, 1.5198177,0.24255922],
 [4.5957584,3.1621256,1.3399473,0.27516776],
         [6.111326, 2.3498883,3.8381867,0.8917184 ],
 [6.076276, 2.7654276,3.6163125,1.3332341 ],
 [5.574494, 3.0580628,4.586999, 1.447556,],
 [5.977648, 2.9043317,4.556511, 1.5003672 ],

 [5.7129784,2.6182246,3.7359285,1.0889959 ],
 [6.084651, 2.685607, 3.767677, 1.3108563 ],
 [5.826792, 2.2109492,2.6229565,0.63938904],
 [5.7655106,2.9755418,4.481058, 1.5090744 ],
 [6.199295, 2.5375395,3.6957557,1.1182954 ],
 [5.8794966,2.7733583,3.9270568,1.3395562 ],
 [5.5756826,3.0872033,4.7123795,1.5388186 ],
 [5.940567, 2.9504883,4.4091682,1.6000552 ],
 [6.1090355,2.6747181,4.16964,1.2059505 ],
 [6.268306, 2.8805332,4.9400134,1.5319948 ],
 [6.2041426,2.603496, 3.956131, 1.1491652 ],
         [6.5665116,2.9059098,5.4836826,1.9638038],
 [6.851912,3.130685,5.8088827,1.92006],
 [7.0135717,2.6604855,5.744461,1.9728545],
 [6.313309,2.799107,5.3165245,1.5990769],
 [6.506722,3.152939,5.651517,1.8390161],
 [7.109249,3.1558397,6.029741,1.8479614],
 [6.532408,3.3058774,5.7502728,1.7907735],
 [6.012671,2.700366,5.007846,1.850464 ],
 [7.2821975,3.1646984,6.1571035,1.8616802],
 [6.515516,2.92335, 5.446859,1.9664358],
 [6.812814,3.0317538,5.713085,2.1989918],
 [7.1164174,3.0725694,6.019477,1.9510623],
 [6.9455295,3.1929631,5.969552,1.8980535],
 [6.1585383,2.739057,5.1387916,2.0359845],
 [6.311773,2.8740163,5.3073754,2.114871 ]
 ]
X_gen = np.array(X_gen)

y_gen = []
for i in range(15):
    temp = [1,0,0]
    y_gen.append(temp)
for j in range(15):
    temp = [0,1,0]
    y_gen.append(temp)
for k in range(15):
    temp = [0,0,1]
    y_gen.append(temp)

y_gen = np.array(y_gen)
results = cross_val_score(estimator, X_gen, y_gen, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))
